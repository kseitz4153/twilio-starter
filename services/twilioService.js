var TwilioService = function() {
    // dependencies
    this.q = require('q');
    this.twilioClient = require('twilio');
    this.accountSid = 'AC6a4277724d5a8f3654a5dc2ece7a0489';
    this.authToken = '4ced67dbac02f05181a4157900b66e4e';
    this.AWS = require("aws-sdk");
    this.AWS.config.loadFromPath('./aws.json');
};

TwilioService.prototype = {


    sendMessage : function(to,from,message){
        var self = this;
        var deferred = this.q.defer();

        var client = this.twilioClient(this.accountSid, this.authToken);

        /*
        client.messages.list(function(err, data) {
            data.messages.forEach(function(message) {
                console.log(message.body);
            });
        });
        */

        console.log("to",to);
        console.log("from", from);
        console.log("message", message);

        client.messages.create({
            to: to,
            from: from,
            body: message,
        }, function (err, message) {
            if(err){
                console.error("Unable to sendMessage with twilio:", JSON.stringify(err, null, 2));
                console.log("message",message);
                return deferred.reject(err);
            }

            console.log('sendMessage with twilio: ' + message.sid);
            deferred.resolve({
                server: 'respond with a resource SMS',
                twilioMessage: message.sid
            });
        });

        return deferred.promise;
    }
};

module.exports = TwilioService;



