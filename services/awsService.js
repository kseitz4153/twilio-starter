var AwsService = function() {
    // dependencies
    this.q = require('q');
    this.AWS = require("aws-sdk");
    this.AWS.config.loadFromPath('./aws.json');
    this.uuid = require("uuid");
    this.AWS.config.update({
        region: "us-east-1"
    });
};

AwsService.prototype = {
    getDocument : function(id){
        var deferred = this.q.defer();

        var docClient = new this.AWS.DynamoDB.DocumentClient();
        var table = "twilio_users";
        var params = {
            TableName: table,
            Key:{
                "id": id
            }
        };

        docClient.get(params, function(err, data) {
            if (err) {
                console.error("Unable to read item. Error JSON:", JSON.stringify(err, null, 2));
                return deferred.reject(err);
            } else {
                console.log("GetItem succeeded:", JSON.stringify(data, null, 2));
                deferred.resolve(data.Item);
            }
        });

        return deferred.promise;
    },
    insertDocument: function(params){
        var deferred = this.q.defer();

        var docClient = new this.AWS.DynamoDB.DocumentClient();

        docClient.put(params, function(err, data) {
            if (err) {
                console.error("Unable to add item for table: " + params.TableName + " Error JSON:", JSON.stringify(err, null, 2));
                return deferred.reject(err);
            } else {
                console.log("Added item to table: " + params.TableName + " with this item: " + JSON.stringify(data, null, 2));
                deferred.resolve(data);
            }
        });

        return deferred.promise;
    },
    queryDocument : function(params){
        var deferred = this.q.defer();

        var docClient = new this.AWS.DynamoDB.DocumentClient();

        docClient.query(params, function(err, data) {
            if (err) {
                console.error("queryDocument: Unable to read item from table: " + params.TableName + ". Error JSON:", JSON.stringify(err, null, 2));
                return deferred.reject(err);
            } else {
                console.log("queryDocument query succeeded:", JSON.stringify(data, null, 2));
                var item = data.Items && data.Items.length > 0 ? data.Items[0] : null;
                deferred.resolve(item);
            }
        });

        return deferred.promise;
    }
};

module.exports = AwsService;



