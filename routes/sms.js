var express = require('express');
var router = express.Router();
var q = require('q');
var uuid = require('uuid');

var AWS = require("aws-sdk");
AWS.config.loadFromPath('./aws.json');
var twilioService = require('../services/twilioService');
var twilio = new twilioService();
var awsService = require('../services/awsService');
var aws = new awsService();
var dynamodb = new AWS.DynamoDB();

router.get('/table', function(req, res, next) {
	params = {
	  TableName: "twilio_users"
	 };
		 dynamodb.describeTable(params, function(err, data) {
		    if (err) console.log(err, err.stack); // an error occurred
		    else     res.send(data);           // successful response})
  });
	})


/* POST send message. */
router.post('/', function(req, res, next) {

    //look up number to
    ///look up number from
    //send message
    //log message

    //then create webhook.
    //deploy?

    var awsQuery = {
        TableName: "twilio_users",
        IndexName: "virtualNumberIndex",
        KeyConditionExpression: "virtualNumber = :phone",
        ExpressionAttributeValues: null,
        Limit: 1
    };
    var message = "";
    var resultsToSend = null;
    var toRealNumber = "";

    //console.log("awsQuery",awsQuery)

    awsQuery.ExpressionAttributeValues = {
        ":phone": req.body.from
    };
    console.log("awsQuery.ExpressionAttributeValues", awsQuery.ExpressionAttributeValues);
    return aws.queryDocument(awsQuery)
        .then(function(fromUser){
            var fromUser = "+18607864269";
            if(!fromUser) throw new Error("Object Not Found For 'from'");
            message = fromUser + " says: " + req.body.message;
            awsQuery.ExpressionAttributeValues = {
                ":phone": req.body.to
            };
            console.log("hit1!");
            //return aws.queryDocument(awsQuery);
        })
        .then(function(toUser){
            console.log("hit2!");
            var toUser = "+13392354155";
            if(!toUser) throw new Error("Object Not Found For 'to'");
            //toRealNumber = toUser.realNumber;
            return twilio.sendMessage(toUser,req.body.from,"hello");
        })
        .then(function(results){
            resultsToSend = results;
            console.log("hit3!");

            var params = {
                TableName: "twilio_messages",
                Item: {
                    id: uuid.v1(),
                    to: req.body.to,
                    toRealNumber: toRealNumber,
                    from: req.body.from,
                    message: req.body.message
                }
            };
            return aws.insertDocument(params);
        })
        .then(function(results){
            res.send(resultsToSend);
        })
        .fail(function(err){
            next(err);
        });
  });

module.exports = router;

//8607864269

// curl -X POST 'https://api.twilio.com/2010-04-01/Accounts/<AccountSid>/Messages.json' \
// --data-urlencode 'To=<ToNumber>' \
// --data-urlencode 'From=<FromNumber>' \
// --data-urlencode 'Body=<BodyText>' \
// -u <AccountSid>:<AuthToken>

