var express = require('express');
var router = express.Router();
var AWS = require("aws-sdk");
var uuid = require("uuid");
AWS.config.loadFromPath('./aws.json');

/* GET users listing. */
router.get('/twiliousers/:id', function(req, res, next) {
    var docClient = new AWS.DynamoDB.DocumentClient();
    var table = "twilio_users";
    var params = {
        TableName: table,
        Key:{
            "id": req.params.id
        }
    };
    docClient.get(params, function(err, data) {
        if (err) {
            console.error("Unable to read item. Error JSON:", JSON.stringify(err, null, 2));
            next(err);
        } else {
            console.log("GetItem succeeded:", JSON.stringify(data, null, 2));
            res.send(data.Item);
        }
    });
  //res.send('respond with a resource');
});

router.post('/twiliousers', function(req, res, next) {
  var itemToPost = {
      id: req.body.name,
      virtualNumber: req.body.virtualNumber,
      realNumber: req.body.realNumber
  };


    var docClient = new AWS.DynamoDB.DocumentClient();
    var table = "twilio_users";
    var params = {
        TableName: table,
        Item: itemToPost
    };
    docClient.put(params, function(err, data) {
        if (err) {
            console.error("Unable to add item. Error JSON:", JSON.stringify(err, null, 2));
            //err.status = 404;
            next(err);
            //res.send("Unable to add item. Error JSON:", JSON.stringify(err, null, 2));
        } else {
            console.log("Added item:", JSON.stringify(data, null, 2));
            res.send(data);
        }
    });
    //res.send('respond with a resource');
});

module.exports = router;
